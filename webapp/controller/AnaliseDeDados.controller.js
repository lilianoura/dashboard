sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./Dialog2",
	"./Texto5C",
	"./utilities",
	"sap/ui/core/routing/History",
	'sap/ui/model/Filter'
], function (BaseController, MessageBox, Dialog2, Texto5C, Utilities, History, Filter) {
	"use strict";

	return BaseController.extend("com.sap.build.h12f10161-us_3.dashboardTabelas.controller.AnaliseDeDados", {
		formataValor: function (oEvt) {
			if (oEvt !== " " && oEvt !== undefined) {
				if (oEvt.slice(oEvt.length - 1, oEvt.length) === "-") {
					var value = oEvt.slice(0, oEvt.length - 1);
					if (!isNaN(value)) {
						value = parseFloat(value) * -1;
						oEvt = parseFloat(value).toFixed(2);
					}
				} else {
					if (!isNaN(oEvt)) {
						oEvt = parseFloat(oEvt).toFixed(2);
					}
				}
			}
			var oLocale = new sap.ui.core.Locale("pt-BR");
			var oFormatOptions = {
				maxFractionDigits: 2
			};
			var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
			oEvt = oFloatFormat.format(oEvt);

			return oEvt;
		},

		formataTipo: function (oEvt) {
			var format = new sap.ui.model.type.Float({
				groupingEnabled: true,
				groupingSeparator: ".",
				minFractionDigits: 2
			});

			return format;

		},

		handleRouteMatched: function (oEvent) {
			var sAppId = "App5d2490b700160954c5f85bec";

			var oParams = {};
			var bSelectFirstListItem = true;
			var partner = oEvent.getParameters().data.partner;
			var oModel = this.getOwnerComponent().getModel();

			this.getView().bindObject({
				path: "/ZFI_DASHBOARD_DETALHES(" +
					"partner='" + partner +
					"')",
				parameters: {
					expand: "to_clienteUnico,to_cliente,to_garantia,to_faturamento,to_fornecedores,to_balanco,to_culturas,to_hist_vendas,to_socio_integrante,to_integrantes,to_contratos"
				}
			});

			var oFilter = [];
			var oTable = this.getView().byId("TabIndFin");
			var oBinding = oTable.getBinding("items");

			var partnerFilter = partner;
			var length = 10;
			while (partnerFilter.length < length) {
				partnerFilter = '0' + partnerFilter;
			}

			var filterPartner = new sap.ui.model.Filter("partner", sap.ui.model.FilterOperator.EQ, partnerFilter);

			var filterArr = [filterPartner];
			var finalFilter = new sap.ui.model.Filter({
				filters: filterArr,
				and: true
			});

			oBinding.filter(finalFilter);

			oModel.read(
				"/ZFI_DASHBOARD_BALANCO_CAMPOSSet", {
					filters: filterArr,
					success: function (oData, response) {
						var oModelAno = this.getView().getModel("IndFinAno");
						var dataBalanco,
							dataTitulo;
						var objectBal = [],
							objectIr = [];
						var titulo;

						for (var cont = 0; cont < oData.results.length; cont++) {

							dataBalanco = {
								"descr_campo": oData.results[cont].descr_campo,
								"valor_ano1": oData.results[cont].valor_ano1,
								"valor_ano2": oData.results[cont].valor_ano2,
								"valor_ano3": oData.results[cont].valor_ano3
							};

							switch (oData.results[cont].campo) {
							case "ANO":
								oModelAno.setProperty("/ano1", oData.results[cont].valor_ano1);
								oModelAno.setProperty("/ano2", oData.results[cont].valor_ano2);
								oModelAno.setProperty("/ano3", oData.results[cont].valor_ano3);
								break;
							case "MARGEM_BRUTA":
							case "MARGEM_OPERACIONAL":
							case "MARGEM_LIQUIDA":
							case "TAXA_ROI":
							case "TAXA_ROE":
							case "GIRO_ATIVO":
							case "LIQUIDEZ_CORRENTE":
							case "LIQUIDEZ_SECA":
							case "LIQUIDEZ_GERAL":
							case "CAPITAL_CIRC_LIQ":
							case "PART_CAPITAL_TERC":
							case "CAPITAL_PROPRIO":
							case "ENDIVIDAMENTO_PL":
							case "ENDIVIDAMENTO_FIN":
							case "ENDIVIDAMENTO_CP":
							case "ENDIVIDAMENTO_LP":

								if (oData.results[cont].campo === "MARGEM_BRUTA" ||
									oData.results[cont].campo === "LIQUIDEZ_CORRENTE" ||
									oData.results[cont].campo === "PART_CAPITAL_TERC") {

									if (oData.results[cont].campo === "MARGEM_BRUTA") {
										titulo = "RENTABILIDADE";
									} else if (oData.results[cont].campo === "LIQUIDEZ_CORRENTE") {
										titulo = "LIQUIDEZ";
									} else {
										titulo = "ENDIVIDAMENTO";
									}

									dataTitulo = {
										"descr_campo": titulo,
										"valor_ano1": " ",
										"valor_ano2": " ",
										"valor_ano3": " "
									};
									objectBal.push(dataTitulo);
								}

								objectBal.push(dataBalanco);
								break;

							case "BENS_TOTAIS":
							case "BENS_IMOVEIS":
							case "AREA_HA":
							case "VALOR_AREA":
							case "DIVIDA":
							case "IMOVEL_EXPLORADO":
							case "RECEITAS":
							case "DESPESAS":
							case "RESULTADOS":
							case "PREJUIZOS_ACUMULADOS":
							case "DIVIDA_ATIV_RURAL":
							case "BOVINOS_QTDE":
							case "BOVINOS_RS":
							case "RECEITA_POR_DIVIDA":
							case "BENS_POR_DIVIDAS":
							case "BENS_AGRIANUAL_DIV":
							case "RECEITA_OPERACIONAL":
							case "CUSTO_OPER_EFETIVO":
							case "RESULTADO_OPER_BRUTO":
							case "DESPESAS_INVESTIMENTOS":
							case "RESULTADO_LIQ":
							case "MARGEM_OPER":
							case "MARGEM_LIQUIDA_IRPF":
							case "LIQUIDEZ_OPER":
							case "PRAZO_REC_VENDAS":
								objectIr.push(dataBalanco);
								break;

							default:
								// code block
							}
						}
						var jsonIndFin = new sap.ui.model.json.JSONModel();
						jsonIndFin.setData(objectBal);
						this.getView().setModel(jsonIndFin, "IndFinan");
						//this.getView().byId("TabIndFin").setModel(jsonIndFin, "IndFinan");

						var jsonIr = new sap.ui.model.json.JSONModel();
						jsonIr.setData(objectIr);
						this.getView().setModel(jsonIr, "DadosIr");
						//this.getView().byId("TabDadosIr").setModel(jsonIr, "DadosIr");

					}.bind(this)
				}
			);

			oModel.read(
				"/ZFI_DASHBOARD_BALANCO_VARIACAOSet", {
					filters: filterArr,
					success: function (oData, response) {
						var oModelBalAno = this.getView().getModel("BalAno");
						var dataBalVar;
						var objectDre = [],
							objectAtivo = [],
							objectPassivo = [];

						var cont,
							totAtivoAno1,
							totAtivoAno2,
							totAtivoAno3,
							totPassivoAno1,
							totPassivoAno2,
							totPassivoAno3;

						for (cont = 0; cont < oData.results.length; cont++) {
							if (oData.results[cont].campo === "ATIVO") {
								totAtivoAno1 = oData.results[cont].valor_ano1;
								totAtivoAno2 = oData.results[cont].valor_ano2;
								totAtivoAno3 = oData.results[cont].valor_ano3;
							} else
							if (oData.results[cont].campo === "PASSIVO") {
								totPassivoAno1 = oData.results[cont].valor_ano1;
								totPassivoAno2 = oData.results[cont].valor_ano2;
								totPassivoAno3 = oData.results[cont].valor_ano3;
							}
						}

						for (cont = 0; cont < oData.results.length; cont++) {

							dataBalVar = {
								"descr_campo": oData.results[cont].descr_campo,
								"valor_ano1": oData.results[cont].valor_ano1,
								"valor_ano2": oData.results[cont].valor_ano2,
								"valor_ano3": oData.results[cont].valor_ano3,
								"analise_v_ano1": oData.results[cont].analise_v_ano1,
								"analise_v_ano2": oData.results[cont].analise_v_ano2,
								"analise_v_ano3": oData.results[cont].analise_v_ano3,
								"analise_h": oData.results[cont].analise_h,
								"variacao": oData.results[cont].variacao
							};

							switch (oData.results[cont].campo) {
							case "ANO":
								oModelBalAno.setProperty("/ano1", oData.results[cont].valor_ano1);
								oModelBalAno.setProperty("/ano2", oData.results[cont].valor_ano2);
								oModelBalAno.setProperty("/ano3", oData.results[cont].valor_ano3);
								break;
							case "RECEITA_VENDAS":
							case "LUCRO_BRUTO":
							case "LUCRO_OPER":
							case "LUCRO_ANTES_IR":
							case "LUCRO_LIQ_EXER":
								objectDre.push(dataBalVar);
								break;
							case "CIRCULANTE":
							case "REALIZAVEL_LP":
							case "PERMANENTE":
								//case "ATIVO":
								objectAtivo.push(dataBalVar);

								if (oData.results[cont].campo === "PERMANENTE") {
									dataBalVar = {
										"descr_campo": "TOTAL DO ATIVO",
										"valor_ano1": totAtivoAno1,
										"valor_ano2": totAtivoAno2,
										"valor_ano3": totAtivoAno3
									};
									objectAtivo.push(dataBalVar);
								}

								break;
							case "CIRCULANTE_PASS":
							case "EXIGIVEL_LP":
							case "PATRIMONIO_LIQ":
							case "CAPITAL_SOCIAL":
								//case "PASSIVO":
								objectPassivo.push(dataBalVar);

								if (oData.results[cont].campo === "CAPITAL_SOCIAL") {
									dataBalVar = {
										"descr_campo": "TOTAL DO PASSIVO",
										"valor_ano1": totPassivoAno1,
										"valor_ano2": totPassivoAno2,
										"valor_ano3": totPassivoAno3
									};
									objectPassivo.push(dataBalVar);
								}

								break;
							default:
								// code block
							}
						}
						var jsonDre = new sap.ui.model.json.JSONModel();
						jsonDre.setData(objectDre);
						this.getView().setModel(jsonDre, "BalDre");
						//this.getView().byId("TabBalDre").setModel(jsonDre, "BalDre");

						var jsonAtivo = new sap.ui.model.json.JSONModel();
						jsonAtivo.setData(objectAtivo);
						this.getView().setModel(jsonAtivo, "BalAtivo");
						//this.getView().byId("TabBalAtivo").setModel(jsonAtivo, "BalAtivo");

						var jsonPassivo = new sap.ui.model.json.JSONModel();
						jsonPassivo.setData(objectPassivo);
						this.getView().setModel(jsonPassivo, "BalPassivo");
						//this.getView().byId("TabBalPassivo").setModel(jsonPassivo, "BalPassivo");

					}.bind(this)
				}
			);

			oModel.read(
				"/ZFI_DASHBOARD_BUDGET_ANOSet", {
					filters: filterArr,
					success: function (oData, response) {
						
						var dataBudgetAno;
						var objectBudgetAno = [];
						
						for (var cont = 0; cont < oData.results.length; cont++) {

							dataBudgetAno = {
								"orcado": oData.results[cont].orcado,
								"realizado": oData.results[cont].realizado,
								"pendente": oData.results[cont].pendente								
							};
						
							objectBudgetAno.push(dataBudgetAno);
						}
						
						var jsonBudgetAno = new sap.ui.model.json.JSONModel();
						jsonBudgetAno.setData(dataBudgetAno);
						this.getView().setModel(jsonBudgetAno, "BudgetAno");
					}.bind(this)
				}
			);

			oModel.read(
				"/ZFI_DASHBOARD_BUDGET_CULTURASet", {
					filters: filterArr,
					success: function (oData, response) {
						
						var dataBudgetCultura;
						var objectBudgetCultura = [];
						
						for (var cont = 0; cont < oData.results.length; cont++) {

							dataBudgetCultura = {
								"cod_cultura": oData.results[cont].cod_cultura,
								"desc_cultura": oData.results[cont].desc_cultura,
								"valor": oData.results[cont].valor,
								"percentual": oData.results[cont].percentual
							};
							
							objectBudgetCultura.push(dataBudgetCultura);
						}
						
						var jsonBudgetCultura = new sap.ui.model.json.JSONModel();
						jsonBudgetCultura.setData(objectBudgetCultura);
						this.getView().setModel(jsonBudgetCultura, "BudgetCultura");
					}.bind(this)
				}
			);
			
			oModel.read(
				"/ZFI_DASHBOARD_GRUPO_FINANCEIROSet", {
					filters: filterArr,
					success: function (oData, response) {
						
						var dataGrupoFinan;
						var objectGrupoFinan = [];
						
						for (var cont = 0; cont < oData.results.length; cont++) {

							dataGrupoFinan = {
								"codigo": oData.results[cont].codigo,
								"nome": oData.results[cont].nome
							};
							
							objectGrupoFinan.push(dataGrupoFinan);
						}
						
						var jsonGrupoFinan = new sap.ui.model.json.JSONModel();
						jsonGrupoFinan.setData(objectGrupoFinan);
						this.getView().setModel(jsonGrupoFinan, "GrupoFinan");
					}.bind(this)
				}
			);
			
			oModel.read(
				"/ZFI_DASHBOARD_POSICAO_CLIENTESet", {
					filters: filterArr,
					success: function (oData, response) {
						
						var dataPosicaoCli;
						var objectPosicaoCli = [];
						
						for (var cont = 0; cont < oData.results.length; cont++) {

							dataPosicaoCli = {
								"vencido": parseFloat(oData.results[cont].vencido).toFixed(2),
								"avencer": parseFloat(oData.results[cont].avencer).toFixed(2),
								"total": parseFloat(oData.results[cont].total).toFixed(2)
							};
							
							objectPosicaoCli.push(dataPosicaoCli);
						}
						
						var jsonGrupoFinan = new sap.ui.model.json.JSONModel();
						jsonGrupoFinan.setData(objectPosicaoCli);
						this.getView().setModel(jsonGrupoFinan, "PosicaoCli");
					}.bind(this)
				}
			);			
			
			// if (oEvent.mParameters.data.context) {
			// 	this.sContext = oEvent.mParameters.data.context;

			// } else {
			// 	if (this.getOwnerComponent().getComponentData()) {
			// 		var patternConvert = function(oParam) {
			// 			if (Object.keys(oParam).length !== 0) {
			// 				for (var prop in oParam) {
			// 					if (prop !== "sourcePrototype" && prop.includes("Set")) {
			// 						return prop + "(" + oParam[prop][0] + ")";
			// 					}
			// 				}
			// 			}
			// 		};

			// 		this.sContext = patternConvert(this.getOwnerComponent().getComponentData().startupParameters);

			// 	}
			// }

			// if (!this.sContext) {
			// 	this.sContext = "LimitedeCreditoSet('1')";
			// }

			// var oPath;

			// if (this.sContext) {
			// 	oPath = {
			// 		path: "/" + this.sContext,
			// 		parameters: oParams
			// 	};
			// 	this.getView().bindObject(oPath);
			// }

		},

		_onPageNavButtonPress: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			var oQueryParams = this.getQueryParameters(window.location);

			if (sPreviousHash !== undefined || oQueryParams.navBackToLaunchpad) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("default", true);
			}

		},
		getQueryParameters: function (oLocation) {
			var oQuery = {};
			var aParams = oLocation.search.substring(1).split("&");
			for (var i = 0; i < aParams.length; i++) {
				var aPair = aParams[i].split("=");
				oQuery[aPair[0]] = decodeURIComponent(aPair[1]);
			}
			return oQuery;

		},
		_onLinkPress: function (oEvent) {

			var sDialogName = "Dialog2";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			if (!oDialog) {
				oDialog = new Dialog2(this.getView());
				this.mDialogs[sDialogName] = oDialog;

				// For navigation.
				oDialog.setRouter(this.oRouter);
			}

			var context = oEvent.getSource().getBindingContext();
			oDialog._oControl.setBindingContext(context);

			oDialog.open();

		},
		_onLinkPress1: function () {
			return new Promise(function (fnResolve) {
				var sTargetPos = "center center";
				sTargetPos = (sTargetPos === "default") ? undefined : sTargetPos;
				sap.m.MessageToast.show("Irá te levar ao S4", {
					onClose: fnResolve,
					duration: 0 || 3000,
					at: sTargetPos,
					my: sTargetPos
				});
			}).catch(function (err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		_onOverflowToolbarButtonPress: function (oEvent) {

			this.mSettingsDialogs = this.mSettingsDialogs || {};
			var sSourceId = oEvent.getSource().getId();
			var oDialog = this.mSettingsDialogs["ViewSettingsDialog1"];

			var confirmHandler = function (oConfirmEvent) {
				var self = this;
				var sFilterString = oConfirmEvent.getParameter('filterString');
				var oBindingData = {};

				/* Grouping */
				if (oConfirmEvent.getParameter("groupItem")) {
					var sPath = oConfirmEvent.getParameter("groupItem").getKey();
					oBindingData.groupby = [new sap.ui.model.Sorter(sPath, oConfirmEvent.getParameter("groupDescending"), true)];
				} else {
					// Reset the group by
					oBindingData.groupby = null;
				}

				/* Sorting */
				if (oConfirmEvent.getParameter("sortItem")) {
					var sPath = oConfirmEvent.getParameter("sortItem").getKey();
					oBindingData.sorters = [new sap.ui.model.Sorter(sPath, oConfirmEvent.getParameter("sortDescending"))];
				}

				/* Filtering */
				oBindingData.filters = [];
				// The list of filters that will be applied to the collection
				var oFilter;
				var vValueLT, vValueGT;

				// Simple filters (String)
				var mSimpleFilters = {},
					sKey;
				for (sKey in oConfirmEvent.getParameter("filterKeys")) {
					var aSplit = sKey.split("___");
					var sPath = aSplit[1];
					var sValue1 = aSplit[2];
					var oFilterInfo = new sap.ui.model.Filter(sPath, "EQ", sValue1);

					// Creating a map of filters for each path
					if (!mSimpleFilters[sPath]) {
						mSimpleFilters[sPath] = [oFilterInfo];
					} else {
						mSimpleFilters[sPath].push(oFilterInfo);
					}
				}

				for (var path in mSimpleFilters) {
					// All filters on a same path are combined with a OR
					oBindingData.filters.push(new sap.ui.model.Filter(mSimpleFilters[path], false));
				}

				aCollections.forEach(function (oCollectionItem) {
					var oCollection = self.getView().byId(oCollectionItem.id);
					var oBindingInfo = oCollection.getBindingInfo(oCollectionItem.aggregation);
					var oBindingOptions = this.updateBindingOptions(oCollectionItem.id, oBindingData, sSourceId);
					if (oBindingInfo.model === "kpiModel") {
						oCollection.getObjectBinding().refresh();
					} else {
						oCollection.bindAggregation(oCollectionItem.aggregation, {
							model: oBindingInfo.model,
							path: oBindingInfo.path,
							parameters: oBindingInfo.parameters,
							template: oBindingInfo.template,
							templateShareable: true,
							sorter: oBindingOptions.sorters,
							filters: oBindingOptions.filters
						});
					}

					// Display the filter string if necessary
					if (typeof oCollection.getInfoToolbar === "function") {
						var oToolBar = oCollection.getInfoToolbar();
						if (oToolBar && oToolBar.getContent().length === 1) {
							oToolBar.setVisible(!!sFilterString);
							oToolBar.getContent()[0].setText(sFilterString);
						}
					}
				}, this);
			}.bind(this);

			function resetFiltersHandler() {

			}

			function updateDialogData(filters) {
				var mParams = {
					context: oReferenceCollection.getBindingContext(),
					success: function (oData) {
						var oJsonModelDialogData = {};
						// Loop through each entity
						oData.results.forEach(function (oEntity) {
							// Add the distinct properties in a map
							for (var oKey in oEntity) {
								if (!oJsonModelDialogData[oKey]) {
									oJsonModelDialogData[oKey] = [oEntity[oKey]];
								} else if (oJsonModelDialogData[oKey].indexOf(oEntity[oKey]) === -1) {
									oJsonModelDialogData[oKey].push(oEntity[oKey]);
								}
							}
						});

						var oDialogModel = oDialog.getModel();

						if (!oDialogModel) {
							oDialogModel = new sap.ui.model.json.JSONModel();
							oDialog.setModel(oDialogModel);
						}
						oDialogModel.setData(oJsonModelDialogData);
						oDialog.open();
					}
				};
				var sPath;
				var sModelName = oReferenceCollection.getBindingInfo(aCollections[0].aggregation).model;
				// In KPI mode for charts, getBindingInfo would return the local JSONModel
				if (sModelName === "kpiModel") {
					sPath = oReferenceCollection.getObjectBinding().getPath();
				} else {
					sPath = oReferenceCollection.getBindingInfo(aCollections[0].aggregation).path;
				}
				mParams.filters = filters;
				oModel.read(sPath, mParams);
			}

			if (!oDialog) {
				oDialog = sap.ui.xmlfragment({
					fragmentName: "com.sap.build.h12f10161-us_3.dashboardTabelas.view.ViewSettingsDialog1"
				}, this);
				oDialog.attachEvent("confirm", confirmHandler);
				oDialog.attachEvent("resetFilters", resetFiltersHandler);

				this.mSettingsDialogs["ViewSettingsDialog1"] = oDialog;
			}

			var aCollections = [];

			aCollections.push({
				id: "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-3-content-sap_ui_layout_BlockLayout-1560274698582-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-build_simple_Table-1",
				aggregation: "items"
			});

			var oReferenceCollection = this.getView().byId(aCollections[0].id);
			var oSourceBindingContext = oReferenceCollection.getBindingContext();
			var oModel = oSourceBindingContext ? oSourceBindingContext.getModel() : this.getView().getModel();

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), oDialog);
			var designTimeFilters = this.mBindingOptions && this.mBindingOptions[aCollections[0].id] && this.mBindingOptions[aCollections[0].id]
				.filters && this.mBindingOptions[aCollections[0].id].filters[undefined];
			updateDialogData(designTimeFilters);

		},
		updateBindingOptions: function (sCollectionId, oBindingData, sSourceId) {
			this.mBindingOptions = this.mBindingOptions || {};
			this.mBindingOptions[sCollectionId] = this.mBindingOptions[sCollectionId] || {};

			var aSorters = this.mBindingOptions[sCollectionId].sorters;
			var aGroupby = this.mBindingOptions[sCollectionId].groupby;

			// If there is no oBindingData parameter, we just need the processed filters and sorters from this function
			if (oBindingData) {
				if (oBindingData.sorters) {
					aSorters = oBindingData.sorters;
				}
				if (oBindingData.groupby || oBindingData.groupby === null) {
					aGroupby = oBindingData.groupby;
				}
				// 1) Update the filters map for the given collection and source
				this.mBindingOptions[sCollectionId].sorters = aSorters;
				this.mBindingOptions[sCollectionId].groupby = aGroupby;
				this.mBindingOptions[sCollectionId].filters = this.mBindingOptions[sCollectionId].filters || {};
				this.mBindingOptions[sCollectionId].filters[sSourceId] = oBindingData.filters || [];
			}

			// 2) Reapply all the filters and sorters
			var aFilters = [];
			for (var key in this.mBindingOptions[sCollectionId].filters) {
				aFilters = aFilters.concat(this.mBindingOptions[sCollectionId].filters[key]);
			}

			// Add the groupby first in the sorters array
			if (aGroupby) {
				aSorters = aSorters ? aGroupby.concat(aSorters) : aGroupby;
			}

			var aFinalFilters = aFilters.length > 0 ? [new sap.ui.model.Filter(aFilters, true)] : undefined;
			return {
				filters: aFinalFilters,
				sorters: aSorters
			};

		},
		getCustomFilter: function (sPath, vValueLT, vValueGT) {
			if (vValueLT !== "" && vValueGT !== "") {
				return new sap.ui.model.Filter([
					new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.GT, vValueGT),
					new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.LT, vValueLT)
				], true);
			}
			if (vValueLT !== "") {
				return new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.LT, vValueLT);
			}
			return new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.GT, vValueGT);

		},
		getCustomFilterString: function (bIsNumber, sPath, sOperator, vValueLT, vValueGT) {
			switch (sOperator) {
			case sap.ui.model.FilterOperator.LT:
				return sPath + (bIsNumber ? ' (Less than ' : ' (Before ') + vValueLT + ')';
			case sap.ui.model.FilterOperator.GT:
				return sPath + (bIsNumber ? ' (More than ' : ' (After ') + vValueGT + ')';
			default:
				if (bIsNumber) {
					return sPath + ' (More than ' + vValueGT + ' and less than ' + vValueLT + ')';
				}
				return sPath + ' (After ' + vValueGT + ' and before ' + vValueLT + ')';
			}

		},
		filterCountFormatter: function (sValue1, sValue2) {
			return sValue1 !== "" || sValue2 !== "" ? 1 : 0;

		},
		_onLinkPressCred: function (oEvent) {

			var filters = [];

			var path = this.getView().getObjectBinding().getPath();
			var cliente = path.substr(33, 7);

			filters.push(new sap.ui.model.Filter({
				path: "partner",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: cliente
			}));

			// filters.push(cliente);

			this.getOwnerComponent().getModel().read(
				"/ZFI_DASHBOARD_DETALHES", {
					filters: filters,
					urlParameters: {
						"$expand": "to_parecer"
					},
					success: function (oData, response) {
						var json = new sap.ui.model.json.JSONModel();
						var sParecer = "";
						var oDados = oData.results;

						for (var i = 0; i < oDados[0].to_parecer.results.length; i++) {
							sParecer = sParecer + oDados[0].to_parecer.results[i].parecer;
						}
						oData.results[0].to_parecer.results[0].parecer = sParecer;

						json.setData(oData.results[0].to_parecer.results[0]);
						this.getView().setModel(json, "parecer");
					}.bind(this)
				}
			);

			var sDialogName = "Dialog2";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			if (!oDialog) {
				oDialog = new Dialog2(this.getView());
				this.mDialogs[sDialogName] = oDialog;

				// For navigation.
				oDialog.setRouter(this.oRouter);
			}

			var context = oEvent.getSource().getBindingContext();
			oDialog._oControl.setBindingContext(context);

			oDialog.open();

		},

		_onLinkPress5C: function (oEvent) {

			var path = this.getView().getObjectBinding().getPath();

			var sAtvCapacidade = "";
			var sAtvCapital = "";
			var sAtvCarater = "";
			var sAtvColateral = "";
			var sAtvCondicoes = "";

			var sGcdCapacidade = "";
			var sGcdCapital = "";
			var sGcdCarater = "";
			var sGcdColateral = "";
			var sGcdCondicoes = "";

			var sCredCapacidade = "";
			var sCredCapital = "";
			var sCredCarater = "";
			var sCredColateral = "";
			var sCredCondicoes = "";

			this.getView().getModel("CincoC").setProperty("/cinco_c", "");
			
			var filters = [];

			var path = this.getView().getObjectBinding().getPath();
			var cliente = path.substr(33, 7);

			filters.push(new sap.ui.model.Filter({
				path: "partner",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: cliente
			}));

			// filters.push(cliente);
			
			this.getOwnerComponent().getModel().read(
				"/ZFI_DASHBOARD_DETALHES", {
					filters: filters,
					urlParameters: {
						"$expand": "to_cinco_c"			

			// this.getOwnerComponent().getModel().read(path, {
			// 	urlParameters: {
			// 		"$expand": "to_cinco_c"
				},
				success: function (oData, response) {
					var oDados = oData.to_cinco_c;
					
					if (oDados.results.length > 0) {
						for (var i = 0; i < oDados.results.length; i++) {
							switch (oDados.results[i].id_texto) {
							case "ATVCAPACIDADE":
								sAtvCapacidade = sAtvCapacidade + oDados.results[i].cinco_c;
								break;
							case "ATVCAPITAL":
								sAtvCapital = sAtvCapital + oDados.results[i].cinco_c;
								break;
							case "ATVCARATER":
								sAtvCarater = sAtvCarater + oDados.results[i].cinco_c;
								break;
							case "ATVCOLATERAL":
								sAtvColateral = sAtvColateral + oDados.results[i].cinco_c;
								break;
							case "ATVCONDICOES":
								sAtvCondicoes = sAtvCondicoes + oDados.results[i].cinco_c;
								break;

							case "GCDCAPACIDADE":
								sGcdCapacidade = sGcdCapacidade + oDados.results[i].cinco_c;
								break;
							case "GCDCAPITAL":
								sGcdCapital = sGcdCapital + oDados.results[i].cinco_c;
								break;
							case "GCDCARATER":
								sGcdCarater = sGcdCarater + oDados.results[i].cinco_c;
								break;
							case "GCDCOLATERAL":
								sGcdColateral = sGcdColateral + oDados.results[i].cinco_c;
								break;
							case "GCDCONDICOES":
								sGcdCondicoes = sGcdCondicoes + oDados.results[i].cinco_c;
								break;

							case "CREDCAPACIDADE":
								sCredCapacidade = sCredCapacidade + oDados.results[i].cinco_c;
								break;
							case "CREDCAPITAL":
								sCredCapital = sCredCapital + oDados.results[i].cinco_c;
								break;
							case "CREDCARATER":
								sCredCarater = sCredCarater + oDados.results[i].cinco_c;
								break;
							case "CREDCOLATERAL":
								sCredColateral = sCredColateral + oDados.results[i].cinco_c;
								break;
							case "CREDCONDICOES":
								sCredCondicoes = sCredCondicoes + oDados.results[i].cinco_c;
								break;
							}
						}
					}
					this.getView().getModel("CincoC").setProperty("/ATVCAPACIDADE", sAtvCapacidade);
					this.getView().getModel("CincoC").setProperty("/ATVCAPITAL", sAtvCapital);
					this.getView().getModel("CincoC").setProperty("/ATVCARATER", sAtvCarater);
					this.getView().getModel("CincoC").setProperty("/ATVCOLATERAL", sAtvColateral);
					this.getView().getModel("CincoC").setProperty("/ATVCONDICOES", sAtvCondicoes);

					this.getView().getModel("CincoC").setProperty("/GCDCAPACIDADE", sGcdCapacidade);
					this.getView().getModel("CincoC").setProperty("/GCDCAPITAL", sGcdCapital);
					this.getView().getModel("CincoC").setProperty("/GCDCARATER", sGcdCarater);
					this.getView().getModel("CincoC").setProperty("/GCDCOLATERAL", sGcdColateral);
					this.getView().getModel("CincoC").setProperty("/GCDCONDICOES", sGcdCondicoes);

					this.getView().getModel("CincoC").setProperty("/CREDCAPACIDADE", sCredCapacidade);
					this.getView().getModel("CincoC").setProperty("/CREDCAPITAL", sCredCapital);
					this.getView().getModel("CincoC").setProperty("/CREDCARATER", sCredCarater);
					this.getView().getModel("CincoC").setProperty("/CREDCOLATERAL", sCredColateral);
					this.getView().getModel("CincoC").setProperty("/CREDCONDICOES", sCredCondicoes);

				}.bind(this)

			});

			// // if (!this.oDialog5C) {
			// // 	this.oDialog5C = sap.ui.xmlfragment("com.sap.build.h12f10161-us_3.dashboardTabelas.view.Texto5C", this);
			// // 	this.getView().addDependent(this.oDialog5C);
			// // }
			// // this.oDialog5C.bindObject(path).open();
			
			var sDialogName = "Texto5C";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			if (!oDialog) {
				oDialog = new Texto5C(this.getView());
				this.mDialogs[sDialogName] = oDialog;

				// For navigation.
				oDialog.setRouter(this.oRouter);
			}

			var context = oEvent.getSource().getBindingContext();
			oDialog._oControl.setBindingContext(context);

			oDialog.open();			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// var filters = [];

			// var path = this.getView().getObjectBinding().getPath();
			// var cliente = path.substr(33, 7);

			// filters.push(new sap.ui.model.Filter({
			// 	path: "partner",
			// 	operator: sap.ui.model.FilterOperator.EQ,
			// 	value1: cliente
			// }));

			// // filters.push(cliente);

			// this.getOwnerComponent().getModel().read(
			// 	"/ZFI_DASHBOARD_DETALHES", {
			// 		filters: filters,
			// 		urlParameters: {
			// 			"$expand": "to_cinco_c"
			// 		},
			// 		success: function (oData, response) {
			// 			var json = new sap.ui.model.json.JSONModel();
			// 			var sTexto = "";
			// 			var oDados = oData.results;

			// 			for (var i = 0; i < oDados[0].to_parecer.results.length; i++) {
			// 				sTexto = sTexto + oDados[0].to_cinco_c.results[i].cinco_c;
			// 			}
			// 			oData.results[0].to_parecer.results[0].cinco_c = sTexto;

			// 			json.setData(oData.results[0].to_cinco_c.results[0]);
			// 			this.getView().setModel(json, "5C");
			// 		}.bind(this)
			// 	}
			// );

			// // if (!this.oDialogParecer) {
			// // 	this.oDialogParecer = sap.ui.xmlfragment("com.sap.build.h12f10161-us_3.dashboardTabelas.view.Parecer");
			// // 	this.getView().addDependent(this.oDialogParecer);
			// // }
			// // this.oDialogParecer.bindObject(path).open();

			// var sDialogName = "Dialog2";
			// this.mDialogs = this.mDialogs || {};
			// var oDialog = this.mDialogs[sDialogName];
			// if (!oDialog) {
			// 	oDialog = new Dialog2(this.getView());
			// 	this.mDialogs[sDialogName] = oDialog;

			// 	// For navigation.
			// 	oDialog.setRouter(this.oRouter);
			// }

			// var context = oEvent.getSource().getBindingContext();
			// oDialog._oControl.setBindingContext(context);

			// oDialog.open();
			
			
		},

		onInit: function () {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("AnaliseDeDados").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

		},

		onExit: function () {

			// to destroy templates for bound aggregations when templateShareable is true on exit to prevent duplicateId issue
			var aControls = [{
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1-content-sap_ui_layout_Grid-1562245513714-content-sap_m_VBox-1562245782290-items-sap_m_VBox-1562274010175-items-build_simple_Table-1562682691853",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_ui_layout_Grid-1558381108178-content-sap_ui_layout_BlockLayout-1558381259355-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_ui_layout_BlockLayout-1558381520590-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-build_simple_Table-1558381541420",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_ui_layout_Grid-1558381108178-content-sap_ui_layout_BlockLayout-1558381259355-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_ui_layout_BlockLayout-1558382866459-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_m_HBox-1-items-build_simple_Table-1",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_ui_layout_BlockLayout-1558448498117-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_ui_layout_BlockLayout-1559584041332-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-build_simple_Table-1",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1-content-sap_ui_layout_Grid-1562245513714-content-sap_m_VBox-1562245803280-items-sap_m_VBox-1562258406778-items-build_simple_Table-1562614175455",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1-content-sap_ui_layout_Grid-1562245513714-content-sap_m_VBox-1562245803280-items-sap_m_VBox-1562258406778-items-build_simple_Table-1562618145145",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1-content-sap_ui_layout_Grid-1562245513714-content-sap_m_VBox-1562245803280-items-sap_m_VBox-1562258406778-items-build_simple_Table-1562618150670",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1-content-sap_ui_layout_Grid-1562245513714-content-sap_m_VBox-1562245803280-items-sap_m_VBox-1562258406778-items-build_simple_Table-1562618153017",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1-content-sap_ui_layout_Grid-1562245513714-content-sap_m_VBox-1562245803280-items-sap_m_VBox-1562258406778-items-build_simple_Table-1562614176446",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560285274074-content-sap_m_ScrollContainer-1-content-sap_ui_layout_BlockLayout-1-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_m_VBox-1562618703905-items-sap_m_VBox-3-items-build_simple_Table-3",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560285274074-content-sap_m_ScrollContainer-1-content-sap_ui_layout_BlockLayout-1-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_m_VBox-1562618703905-items-sap_m_VBox-3-items-build_simple_Table-4",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560285274074-content-sap_m_ScrollContainer-1-content-sap_ui_layout_BlockLayout-1-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_m_VBox-1562618703905-items-sap_m_VBox-3-items-build_simple_Table-5",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560285274074-content-sap_m_ScrollContainer-1-content-sap_ui_layout_BlockLayout-1-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_m_VBox-1562618703905-items-sap_m_VBox-3-items-build_simple_Table-6",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560285274074-content-sap_m_ScrollContainer-1-content-sap_ui_layout_BlockLayout-1-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_m_VBox-1562618703905-items-sap_m_VBox-3-items-build_simple_Table-7",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-3-content-sap_ui_layout_BlockLayout-1560274698582-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-build_simple_Table-1",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560274808274-content-sap_ui_layout_BlockLayout-1560344525596-content-sap_ui_layout_BlockLayoutRow-2-content-sap_ui_layout_BlockLayoutCell-1-content-build_simple_Table-1562693203745",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560274822970-content-sap_ui_layout_BlockLayout-1560345592299-content-sap_ui_layout_BlockLayoutRow-2-content-sap_ui_layout_BlockLayoutCell-1-content-build_simple_Table-1562693269645",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560274829938-content-sap_ui_layout_BlockLayout-1560278216796-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_m_HBox-2-items-build_simple_Table-1",
				"groups": ["items"]
			}, {
				"controlId": "sap_Responsive_Page_0-content-sap_m_IconTabBar-1560258768827-items-sap_m_IconTabFilter-1560277253232-content-sap_ui_layout_BlockLayout-1560278345579-content-sap_ui_layout_BlockLayoutRow-1-content-sap_ui_layout_BlockLayoutCell-1-content-sap_m_VBox-1562618945058-items-sap_m_VBox-8-items-build_simple_Table-2",
				"groups": ["items"]
			}];
			for (var i = 0; i < aControls.length; i++) {
				var oControl = this.getView().byId(aControls[i].controlId);
				if (oControl) {
					for (var j = 0; j < aControls[i].groups.length; j++) {
						var sAggregationName = aControls[i].groups[j];
						var oBindingInfo = oControl.getBindingInfo(sAggregationName);
						if (oBindingInfo) {
							var oTemplate = oBindingInfo.template;
							oTemplate.destroy();
						}
					}
				}
			}

		}
	});
}, /* bExport= */ true);