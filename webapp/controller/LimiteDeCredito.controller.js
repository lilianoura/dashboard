sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./Observacao",
	"./ObsLimCred",
	"./utilities",
	"sap/ui/core/routing/History",
	"sap/ui/core/Fragment",
	'sap/ui/model/Filter',
	"sap/m/MessageToast"
], function (BaseController, MessageBox, Observacao, ObsLimCred, Utilities, History, Fragment, Filter, MessageToast) {
	"use strict";

	return BaseController.extend("com.sap.build.h12f10161-us_3.dashboardTabelas.controller.LimiteDeCredito", {
		handleRouteMatched: function (oEvent) {
			var sAppId = "App5d2490b700160954c5f85bec";

			var oParams = {};

			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;

			} else {
				if (this.getOwnerComponent().getComponentData()) {
					var patternConvert = function (oParam) {
						if (Object.keys(oParam).length !== 0) {
							for (var prop in oParam) {
								if (prop !== "sourcePrototype" && prop.includes("Set")) {
									return prop + "(" + oParam[prop][0] + ")";
								}
							}
						}
					};

					this.sContext = patternConvert(this.getOwnerComponent().getComponentData().startupParameters);

				}
			}

			var oPath;

			if (this.sContext) {
				oPath = {
					path: "/" + this.sContext,
					parameters: oParams
				};
				this.getView().bindObject(oPath);
			}

		},

		onAfterRendering: function () {
			var oModel = this.getView().getModel(),
				oAutComite = this.getView().getModel("AutComite"),
				oModelComite = this.getView().getModel("Comite");

			oAutComite.setProperty("/autorizado", false);

			oModel.read("/ZFI_DASHBOARD_AUTORIZA_COMITE", {
				success: function (oData, response) {
					if (oData.results[0].autorizado === '') {
						oAutComite.setProperty("/autorizado", false);
						oModelComite.destroy();
					} else {
						oAutComite.setProperty("/autorizado", true);
					}
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},

		handleIconTabBarSelect: function (oEvent) {

			var sKey = oEvent.getParameter("key"),
				oTable,
				oBinding,
				aFilters = [],
				oFilterStatus,
				oFilterComite;

			if (sKey === "EMAPR") {
				oTable = this.getView().byId("TableEmApr");
				oBinding = oTable.getBinding("rows");
				oFilterStatus = ([new Filter("status_", "EQ", "EMAPR")], true);
				aFilters = oFilterStatus;
			} else if (sKey === "COM") {
				oTable = this.getView().byId("TableCom");
				oBinding = oTable.getBinding("rows");
				oFilterComite = ([new Filter("enviado_comite", "EQ", "true")], true);
				aFilters = oFilterComite;
			}
			oBinding.filter(aFilters);
		},

		onListSelect: function (oEvent) {

			var tab = this.getView().byId("TabBar").getSelectedKey();
			var oModel,
				pathStatus,
				path;

			if (tab === "EMAPR") {
				oModel = this.getView().getModel();
				pathStatus = oEvent.getSource().getSelectedItem().getBindingContext().getPath();
				path = oEvent.getSource().getBindingContext().getPath();
			} else {
				oModel = this.getView().getModel("Comite");
				pathStatus = oEvent.getSource().getSelectedItem().getBindingContext("Comite").getPath();
				path = oEvent.getSource().getBindingContext("Comite").getPath();
			}

			var enviadoComite = oModel.getProperty(path).enviado_comite;
			var acao = oModel.getProperty(pathStatus).acao;

			if (acao === "2") {
				var sDialogName = "Observacao";
				this.mDialogs = this.mDialogs || {};
				var oDialog = this.mDialogs[sDialogName];
				if (!oDialog) {
					oDialog = new Observacao(this.getView());
					this.mDialogs[sDialogName] = oDialog;
					// For navigation.
					oDialog.setRouter(this.oRouter);
					this.getView().addDependent(this.oDialog);
				}

				//var context = oEvent.getSource().getBindingContext();
				//oDialog._oControl.setBindingContext(context);
				oDialog._oControl.bindObject({
					// path: context.getPath()
					path: path
				});
				oDialog.open();
			} else if (acao === "3") {
				enviadoComite = true;
			}

			oModel.setProperty(path + "/acao", acao);
			oModel.setProperty(path + "/enviado_comite", enviadoComite);

		},

		onOpenDialogObs: function (oEvent) {
			var tab = this.getView().byId("TabBar").getSelectedKey();
			var path;

			if (tab === "EMAPR") {
				path = oEvent.getSource().getBindingContext().getPath();
			} else {
				path = oEvent.getSource().getBindingContext("Comite").getPath();
			}

			var sDialogName = "ObsLimCred";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			if (!oDialog) {
				oDialog = new ObsLimCred(this.getView());
				this.mDialogs[sDialogName] = oDialog;
				// For navigation.
				oDialog.setRouter(this.oRouter);
				this.getView().addDependent(this.oDialog);
			}

			oDialog._oControl.bindObject({
				path: path
			});
			oDialog.open();
		},

		_onTodosButtonPress: function (oEvent) {

			var tab = this.getView().byId("TabBar").getSelectedKey();
			var oModel;

			if (tab === "EMAPR") {
				oModel = this.getView().getModel();
			} else {
				oModel = this.getView().getModel("Comite");
			}

			//var listItems = oEvent.getSource().getParent().getParent().getRows();
			var listItems = oEvent.getSource().getParent().getParent().getBindingInfo("rows").binding.aKeys;
			//var key = this.getView().byId("TabBar").getSelectedKey();
			var length = listItems.length;
			var path,
				acao,
				acaoNew;

			for (var cont = 0; cont < length; cont++) {
				//path = listItems[cont].getBindingContext().sPath;
				path = "/" + listItems[cont];
				//acao = this.getView().getModel().getProperty(path).acao; 
				acao = oModel.getProperty(path).acao;

				if (acao === "0") {
					acaoNew = "1";
				} else {
					acaoNew = "0";
				}

				oModel.setProperty(path + "/acao", acaoNew);

			};
			//	this.getView().getModel().refresh();
		},

		_onChangeValor: function (oEvent) {
			var tab = this.getView().byId("TabBar").getSelectedKey();
			var oModel,
				sPath;

			if (tab === "EMAPR") {
				oModel = this.getView().getModel();
				sPath = oEvent.getSource().getBindingContext().sPath;
			} else {
				oModel = this.getView().getModel("Comite");
				sPath = oEvent.getSource().getBindingContext("Comite").sPath;
			}

			var credLimVig = this.getView().getModel().getData(sPath).cred_lim_vig;
			var emAprovacao = oEvent.getSource().getValue();

			if (emAprovacao === undefined || emAprovacao === "") {
				emAprovacao = parseFloat("0.00").toFixed(2);
			} else {
				emAprovacao = parseFloat(oModel.getProperty(sPath).em_aprovacao).toFixed(2);
			}

			var credLimDif = parseFloat(credLimVig).toFixed(2) - emAprovacao;

			if (credLimDif === undefined || credLimDif === "") {
				credLimDif = parseFloat("0.00").toFixed(2);
			} else {
				credLimDif = parseFloat(credLimDif).toFixed(2);
			}

			oModel.setProperty(sPath + "/em_aprovacao", emAprovacao);
			oModel.setProperty(sPath + "/cred_lim_dif", credLimDif);
		},

		_onCheck: function (oEvent) {
			var tab = this.getView().byId("TabBar").getSelectedKey();
			var oModel,
				sPath;

			if (tab === "EMAPR") {
				oModel = this.getView().getModel();
				sPath = oEvent.getSource().getBindingContext().sPath;
			} else {
				// oModel = this.getView().getModel("Comite");
				// sPath = oEvent.getSource().getBindingContext("Comite").sPath;
			}

			var emAnalise = oEvent.getParameter("selected");

			oModel.setProperty(sPath + "/em_analise", emAnalise);

		},

		_onSaveButtonPress: function (oEvent) {

			var tab = this.getView().byId("TabBar").getSelectedKey();
			var oModel;

			// if (tab === "EMAPR") {
			var oModelDefault = this.getView().getModel();
			// } else {
			var oModelComite = this.getView().getModel("Comite");
			// }

			// var pending = this.getView().getModel().getPendingChanges();
			// var list = oEvent.getSource().getParent();

			// var key = this.getView().byId("TabBar").getSelectedKey();

			// this.getView().getModel().submitChanges({
			// 	success: function (oData, sResponse) {},
			// 	error: function (oError) {}
			// });
			if (oModelDefault.hasPendingChanges()) {
				oModelDefault.submitChanges({
					success: function (oData, sResponse) {
						MessageToast.show("Registros atualizados com sucesso");
					},
					error: function (oError) {}
				});
			}
			if (oModelComite.hasPendingChanges()) {
				oModelComite.submitChanges({
					success: function (oData, sResponse) {
						MessageToast.show("Registros atualizados com sucesso");
					},
					error: function (oError) {}
				});
			}

			//oModel.refresh();
		},

		_onPageNavButtonPress: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			var oQueryParams = this.getQueryParameters(window.location);

			if (sPreviousHash !== undefined || oQueryParams.navBackToLaunchpad) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("default", true);
			}

		},
		getQueryParameters: function (oLocation) {
			var oQuery = {};
			var aParams = oLocation.search.substring(1).split("&");
			for (var i = 0; i < aParams.length; i++) {
				var aPair = aParams[i].split("=");
				oQuery[aPair[0]] = decodeURIComponent(aPair[1]);
			}
			return oQuery;

		},
		_onTableItemPress: function (oEvent) {

			var sPath = oEvent.getParameter("rowBindingContext").sPath;

			var tab = this.getView().byId("TabBar").getSelectedKey();
			var oModel;

			if (tab === "EMAPR") {
				oModel = this.getView().getModel();
			} else {
				oModel = this.getView().getModel("Comite");
			}

			var path = sPath.slice(1, sPath.length);
			var oData = oModel.oData[path];

			this.oRouter.navTo("AnaliseDeDados", {
				partner: oData.partner,
				aprovador: oData.aprovador,
				nivel: oData.nivel,
				sequencial: oData.sequencial
			}, false);

			// return new Promise(function (fnResolve) {
			// 	this.doNavigate("AnaliseDeDados", oBindingContext, fnResolve, "");
			// }.bind(this)).catch(function (err) {
			// 	if (err !== undefined) {
			// 		MessageBox.error(err.message);
			// 	}
			// });

		},
		doNavigate: function (sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function (bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}

		},

		_onOverflowToolbarButtonPress: function (oEvent) {

			var tab = this.getView().byId("TabBar").getSelectedKey();
			var oModel_,
				oTable;

			if (tab === "EMAPR") {
				oModel_ = this.getView().getModel();
				oTable = "TableEmApr";
			} else {
				oModel_ = this.getView().getModel("Comite");
				oTable = "TableCom";
			}

			this.mSettingsDialogs = this.mSettingsDialogs || {};
			var sSourceId = oEvent.getSource().getId();
			var oDialog = this.mSettingsDialogs["ViewSettingsDialog1"];

			var confirmHandler = function (oConfirmEvent) {
				var self = this;
				var sFilterString = oConfirmEvent.getParameter('filterString');
				var oBindingData = {};

				/* Grouping */
				if (oConfirmEvent.getParameter("groupItem")) {
					var sPath = oConfirmEvent.getParameter("groupItem").getKey();
					oBindingData.groupby = [new sap.ui.model.Sorter(sPath, oConfirmEvent.getParameter("groupDescending"), true)];
				} else {
					// Reset the group by
					oBindingData.groupby = null;
				}

				/* Sorting */
				if (oConfirmEvent.getParameter("sortItem")) {
					var sPath = oConfirmEvent.getParameter("sortItem").getKey();
					oBindingData.sorters = [new sap.ui.model.Sorter(sPath, oConfirmEvent.getParameter("sortDescending"))];
				}

				/* Filtering */
				oBindingData.filters = [];
				// The list of filters that will be applied to the collection
				var oFilter;
				// var vValueLT, vValueGT;
				// var vValue;

				//   vValue = oDialog.getModel().getProperty("/filterPartner/vValue");			    
				//   vValue = oDialog.getModel().getProperty("/filterCidade/vValue");
				//   vValue = oDialog.getModel().getProperty("/filterEstado/vValue");			    

				// Simple filters (String)
				var mSimpleFilters = {},
					sKey;
				for (sKey in oConfirmEvent.getParameter("filterKeys")) {
					var aSplit = sKey.split("___");
					var sPath = aSplit[1];
					var sValue1 = aSplit[2];
					var oFilterInfo = new sap.ui.model.Filter(sPath, "EQ", sValue1);

					// Creating a map of filters for each path
					if (!mSimpleFilters[sPath]) {
						mSimpleFilters[sPath] = [oFilterInfo];
					} else {
						mSimpleFilters[sPath].push(oFilterInfo);
					}
				}

				for (var path in mSimpleFilters) {
					// All filters on a same path are combined with a OR
					oBindingData.filters.push(new sap.ui.model.Filter(mSimpleFilters[path], false));
				}

				aCollections.forEach(function (oCollectionItem) {
					var oCollection = self.getView().byId(oCollectionItem.id);
					var oBindingInfo = oCollection.getBindingInfo(oCollectionItem.aggregation);
					var oBindingOptions = this.updateBindingOptions(oCollectionItem.id, oBindingData, sSourceId);
					if (oBindingInfo.model === "kpiModel") {
						oCollection.getObjectBinding().refresh();
					} else {
						oCollection.bindAggregation(oCollectionItem.aggregation, {
							model: oBindingInfo.model,
							path: oBindingInfo.path,
							parameters: oBindingInfo.parameters,
							template: oBindingInfo.template,
							templateShareable: true,
							sorter: oBindingOptions.sorters,
							filters: oBindingOptions.filters
						});
					}

					// Display the filter string if necessary
					if (typeof oCollection.getInfoToolbar === "function") {
						var oToolBar = oCollection.getInfoToolbar();
						if (oToolBar && oToolBar.getContent().length === 1) {
							oToolBar.setVisible(!!sFilterString);
							oToolBar.getContent()[0].setText(sFilterString);
						}
					}
				}, this);
			}.bind(this);

			function resetFiltersHandler() {
				// oDialog.getModel().setProperty("/filterPartner/vValue", "");
				// oDialog.getModel().setProperty("/filterCidade/vValue", "");
				// oDialog.getModel().setProperty("/filterEstado/vValue", "");

				oDialog.getModel().setProperty("/filterPartner", "");
				oDialog.getModel().setProperty("/filterDescricao", "");
				oDialog.getModel().setProperty("/filterCidade", "");
				oDialog.getModel().setProperty("/filterEstado", "");
				oDialog.getModel().setProperty("/filterCred_lim_vig", "");
				oDialog.getModel().setProperty("/filterCred_lim_dif", "");
				oDialog.getModel().setProperty("/filterRating_ant", "");
				oDialog.getModel().setProperty("/filterRating_atual", "");
				oDialog.getModel().setProperty("/filterRecuperacao_jud", "");
				oDialog.getModel().setProperty("/filterTerritorio", "");
			}

			function updateDialogData(tabela, filters) {
				var mParams = {
					context: oReferenceCollection.getBindingContext(),
					success: function (oData) {
						var oJsonModelDialogData = {};
						// Loop through each entity
						oData.results.forEach(function (oEntity) {
							// Add the distinct properties in a map
							for (var oKey in oEntity) {
								if (!oJsonModelDialogData[oKey]) {
									oJsonModelDialogData[oKey] = [oEntity[oKey]];
								} else if (oJsonModelDialogData[oKey].indexOf(oEntity[oKey]) === -1) {
									oJsonModelDialogData[oKey].push(oEntity[oKey]);
								}
							}
						});

						var oDialogModel = oDialog.getModel();

						// oJsonModelDialogData["filterPartner"] = {
						// 	vValue: (oDialogModel && oDialogModel.getProperty("/partner")) ? oDialogModel.getProperty("/partner/vValue") : "",
						// };

						// oJsonModelDialogData["filterCidade"] = {
						// 	vValue: (oDialogModel && oDialogModel.getProperty("/cidade")) ? oDialogModel.getProperty("/cidade/vValue") : "",
						// };

						// oJsonModelDialogData["filterEstado"] = {
						// 	vValue: (oDialogModel && oDialogModel.getProperty("/estado")) ? oDialogModel.getProperty("/estado/vValue") : "",
						// };

						if (!oDialogModel) {
							oDialogModel = new sap.ui.model.json.JSONModel();
							oDialog.setModel(oDialogModel);
						}
						oDialogModel.setData(oJsonModelDialogData);
						oDialog.open();
					}
				};
				var sPath;
				var sModelName = oReferenceCollection.getBindingInfo(aCollections[0].aggregation).model;
				// In KPI mode for charts, getBindingInfo would return the local JSONModel
				if (sModelName === "kpiModel") {
					sPath = oReferenceCollection.getObjectBinding().getPath();
				} else {
					sPath = oReferenceCollection.getBindingInfo(aCollections[0].aggregation).path;
				}

				filters = [];
				filters.push(new sap.ui.model.Filter("to_limcred_wf/status", sap.ui.model.FilterOperator.EQ, "EMAPR"));
				if (tabela === "EMAPR") {
					filters.push(new sap.ui.model.Filter("to_limcred_wf/enviado_comite", sap.ui.model.FilterOperator.EQ, false));
				} else {
					filters.push(new sap.ui.model.Filter("to_limcred_wf/enviado_comite", sap.ui.model.FilterOperator.EQ, true));
				}
				mParams.filters = filters;
				oModel.read(sPath, mParams);
			}

			if (!oDialog) {
				oDialog = sap.ui.xmlfragment({
					fragmentName: "com.sap.build.h12f10161-us_3.dashboardTabelas.view.ViewSettingsDialog1"
				}, this);
				oDialog.attachEvent("confirm", confirmHandler);
				oDialog.attachEvent("resetFilters", resetFiltersHandler);

				this.mSettingsDialogs["ViewSettingsDialog1"] = oDialog;
			}

			var aCollections = [];

			aCollections.push({
				//id: "sap_Responsive_Page_0-content-build_simple_Table-1558377624612",
				id: oTable,
				aggregation: "rows"
			});

			var oReferenceCollection = this.getView().byId(aCollections[0].id);
			var oSourceBindingContext = oReferenceCollection.getBindingContext();
			var oModel = oSourceBindingContext ? oSourceBindingContext.getModel() : oModel_;

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), oDialog);
			var designTimeFilters = this.mBindingOptions && this.mBindingOptions[aCollections[0].id] && this.mBindingOptions[aCollections[0].id]
				.filters && this.mBindingOptions[aCollections[0].id].filters[undefined];
			updateDialogData(tab, designTimeFilters);

		},
		updateBindingOptions: function (sCollectionId, oBindingData, sSourceId) {
			this.mBindingOptions = this.mBindingOptions || {};
			this.mBindingOptions[sCollectionId] = this.mBindingOptions[sCollectionId] || {};

			var aSorters = this.mBindingOptions[sCollectionId].sorters;
			var aGroupby = this.mBindingOptions[sCollectionId].groupby;

			// If there is no oBindingData parameter, we just need the processed filters and sorters from this function
			if (oBindingData) {
				if (oBindingData.sorters) {
					aSorters = oBindingData.sorters;
				}
				if (oBindingData.groupby || oBindingData.groupby === null) {
					aGroupby = oBindingData.groupby;
				}
				// 1) Update the filters map for the given collection and source
				this.mBindingOptions[sCollectionId].sorters = aSorters;
				this.mBindingOptions[sCollectionId].groupby = aGroupby;
				this.mBindingOptions[sCollectionId].filters = this.mBindingOptions[sCollectionId].filters || {};
				this.mBindingOptions[sCollectionId].filters[sSourceId] = oBindingData.filters || [];
			}

			// 2) Reapply all the filters and sorters
			var aFilters = [];
			for (var key in this.mBindingOptions[sCollectionId].filters) {
				aFilters = aFilters.concat(this.mBindingOptions[sCollectionId].filters[key]);
			}

			// Add the groupby first in the sorters array
			if (aGroupby) {
				aSorters = aSorters ? aGroupby.concat(aSorters) : aGroupby;
			}

			var aFinalFilters = aFilters.length > 0 ? [new sap.ui.model.Filter(aFilters, true)] : undefined;
			return {
				filters: aFinalFilters,
				sorters: aSorters
			};

		},
		// getCustomFilter: function(sPath, vValueLT, vValueGT) {
		// 	if (vValueLT !== "" && vValueGT !== "") {
		// 		return new sap.ui.model.Filter([
		// 			new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.GT, vValueGT),
		// 			new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.LT, vValueLT)
		// 		], true);
		// 	}
		// 	if (vValueLT !== "") {
		// 		return new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.LT, vValueLT);
		// 	}
		// 	return new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.GT, vValueGT);

		// },
		// getCustomFilterString: function(bIsNumber, sPath, sOperator, vValueLT, vValueGT) {
		// 	switch (sOperator) {
		// 		case sap.ui.model.FilterOperator.LT:
		// 			return sPath + (bIsNumber ? ' (Less than ' : ' (Before ') + vValueLT + ')';
		// 		case sap.ui.model.FilterOperator.GT:
		// 			return sPath + (bIsNumber ? ' (More than ' : ' (After ') + vValueGT + ')';
		// 		default:
		// 			if (bIsNumber) {
		// 				return sPath + ' (More than ' + vValueGT + ' and less than ' + vValueLT + ')';
		// 			}
		// 			return sPath + ' (After ' + vValueGT + ' and before ' + vValueLT + ')';
		// 	}

		// },
		filterCountFormatter: function (sValue1, sValue2) {
			return sValue1 !== "" || sValue2 !== "" ? 1 : 0;

		},

		onInit: function () {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("LimiteDeCredito").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},

		onExit: function () {

			// to destroy templates for bound aggregations when templateShareable is true on exit to prevent duplicateId issue
			var aControls = [{
				//"controlId": "sap_Responsive_Page_0-content-build_simple_Table-1558377624612",
				"controlId": "TabBar",
				"groups": ["rows"]
			}];
			for (var i = 0; i < aControls.length; i++) {
				var oControl = this.getView().byId(aControls[i].controlId);
				if (oControl) {
					for (var j = 0; j < aControls[i].groups.length; j++) {
						var sAggregationName = aControls[i].groups[j];
						var oBindingInfo = oControl.getBindingInfo(sAggregationName);
						if (oBindingInfo) {
							var oTemplate = oBindingInfo.template;
							oTemplate.destroy();
						}
					}
				}
			}
		}
	});
}, /* bExport= */ true);